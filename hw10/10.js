const tabs = document.querySelector(".tabs");
const content = [...document.querySelector(".tabs-content").children];
const actTab = document.querySelector(".active").textContent;

function activeTab(params) {
    content.forEach((item) => {
        if (item.dataset.tab === params) {
            item.style.display = "";
        } else {
            item.style.display = "none";
        }
    });
}

function tabChange() {
    tabs.addEventListener("click", (event) => {
        document.querySelector(".active").classList.remove("active");
        event.target.classList.add("active");
        activeTab(event.target.textContent);
    });
}

tabChange();
