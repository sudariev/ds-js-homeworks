// 1) Метод слугує для перебору масиву. Для кожного з його елементів він почергово
// викликає функцію і передає в неї 3 аргументи - поточний елемент масива, його номер та власне сам масив.

// 2) Якщо масиву задати довжину 0 (arr.length = 0), то він очиститься.

// 3) variable instanceof Array.

function filterBy(array, check) {
    newArray = array.filter(function (item) {
        if (check === "null") {
            return item !== null;
        } else if (check === "object") {
            return typeof item !== "object" || item === null;
        } else {
            return typeof item !== check;
        }
    });
    return newArray;
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));
