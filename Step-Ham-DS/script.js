/* Tabs в секціі "Container Tabs" */
const tabTitle = document.querySelectorAll(".tabs-title-item");
const lengthTabTitile = tabTitle.length;
const tabSwitcher = () => {
    return function () {
        let tabContent = document.querySelector(
            `.tabs-content-item[data-content="${this.dataset.content}"]`
        );
        document
            .querySelector(".tabs-title-item.active")
            .classList.remove("active");
        document
            .querySelector(".tabs-content-item.active")
            .classList.remove("active");
        tabContent.classList.add("active");
        this.classList.add("active");
    };
};

for (let i = 0; i < lengthTabTitile; i++) {
    tabTitle[i].addEventListener("click", tabSwitcher());
}

/* Секція "Works". Фільтр фото за категоріями */
const photoTabs = document.querySelectorAll(".works-title-item");
const worksCard = document.querySelectorAll(".works-gallery-card");
worksTabsList.onclick = (e) => {
    let target = e.target;
    photoTabs.forEach((elem) => {
        elem.classList.remove("active");
    });
    target.classList.add("active");
    let tabCategory = target.dataset.content;
    worksCard.forEach((e) => {
        e.classList.add("hidden");
        let cardCategory = e.dataset.content;
        if (tabCategory === "All") {
            e.classList.remove("hidden");
        } else if (tabCategory === cardCategory) {
            e.classList.remove("hidden");
        }
    });
};

/* Обробник на кнопку "Load More" */
loadMorePhoto.onclick = () => {
    photoTabs.forEach((el) => {
        if (el.classList.contains("active")) {
            let a = el.getAttribute("data-content");
            worksCard.forEach((element) => {
                let we = element.getAttribute("data-content");
                if (a === we) {
                    element.classList.remove("hidden");
                } else if (a === "All") {
                    element.classList.remove("hidden");
                }
            });
        }
    });
    loadMorePhoto.remove();
};

/* Присвоює категорії карточкам зображень */

showCategory = () => {
    for (let i = 0; i < worksCard.length; i++) {
        let show = worksCard[i].querySelector(".img-card-category");
        show.textContent = worksCard[i].dataset.content;
    }
};
showCategory();

/* Slider */

let currentSlide = 0;
const navigation = document.querySelectorAll(".slider-user-photo-small");
const slides = document.querySelectorAll(".slider-user-review");
const next = document.getElementById("arrowRight");
const previous = document.getElementById("arrowLeft");

for (let i = 0; i < navigation.length; i++) {
    navigation[i].onclick = function () {
        currentSlide = i;
        document
            .querySelector(".slider-user-review.container.active")
            .classList.remove("active");
        document
            .querySelector(".slider-user-photo-small.active")
            .classList.remove("active");
        navigation[currentSlide].classList.add("active");
        slides[currentSlide].classList.add("active");
    };
}

next.onclick = function () {
    nextSlide(currentSlide);
};

previous.onclick = function () {
    previousSlide(currentSlide);
};

function nextSlide() {
    goToSlide(currentSlide + 1);
}

function previousSlide() {
    goToSlide(currentSlide - 1);
}

function goToSlide(n) {
    hideSlides();
    currentSlide = (n + slides.length) % slides.length;
    showSlides();
}

function hideSlides() {
    slides[currentSlide].className = "slider-user-review container";
    navigation[currentSlide].className = "slider-user-photo-small";
}

function showSlides() {
    slides[currentSlide].className = "slider-user-review container active";
    navigation[currentSlide].className = "slider-user-photo-small active";
}

/* Актуальна дата в Breaking News */

const dateToday = document.querySelectorAll(".news-date");
const date = new Date();
const today = date.toUTCString().slice(5, 11);
dateToday.forEach((e) => {
    e.textContent = `${today}`;
});
