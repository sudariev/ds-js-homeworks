// 1) Які існують типи даних у Javascript?
// Number, BigInt, String, Boolean, null, undefined, NaN, Symbol, Object.

// 2) У чому різниця між == і ===?
// Суворе порівняння перевіряє тотожність значень без приведення типів.

// 3) Що таке оператор?
// Оператор - це команда, що виконує певні інструкції (порівняння, операціі обчислення, дії, логіку тощо).

const username = prompt("Enter username", "username");
const age = +prompt("Enter age", 17);

if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (age <= 22) {
    confirm("Are you sure you want to continue?")
        ? alert(`Welcome, ${username}`)
        : alert("You are not allowed to visit this website");
} else {
    alert(`Welcome, ${username}`);
}
