// 1.Функціі в програмуванні потрібні для виконання різноманітних задач - математичних обчислень, перетворень, дій із змінними та обʼєктами.
// 2. Аргументи в функцію передаються для роботи функції з різними даними (значеннями, змінними), які можуть неодноразово змінюватись.
// Таким чином, функції можна використовувати повторно з різними значеннми та цілями.
// 3. Оператор функції return завершує роботу фінкції та повертає її значення або "undefined", якщо вираз функції не був зазначений.

const firstNumber = +prompt("Enter first number");
const secondNumber = +prompt("Enter second number");
const operation = prompt("Enter desired operation: +, -, *, /");

const firstNum = firstNumber;
const secondNum = secondNumber;
const oper = operation;

function calc(firstNum, secondNum, oper) {
    switch (oper) {
        case "+":
            return "summ = " + (firstNum + secondNum);
        case "-":
            return "substraction = " + (firstNum - secondNum);
        case "*":
            return "multiplication = " + firstNum * secondNum;
        case "/":
            return "division = " + firstNum / secondNum;
        default:
            alert("You haven't enter operation.");
    }
}
console.log(calc(firstNum, secondNum, oper));
