const icons = [...document.getElementsByClassName("fas")];
const btn = document.querySelector(".btn");

function switchIconsAndShowPass() {
    icons.forEach((element) => {
        element.addEventListener("click", (event) => {
            event.target.classList.toggle("fa-eye");
            event.target.classList.toggle("fa-eye-slash");
            if (event.target.classList.contains("fa-eye")) {
                event.target.previousElementSibling.type = "";
            } else {
                event.target.previousElementSibling.type = "password";
            }
        });
    });
}

function getPasses() {
    btn.addEventListener("click", (event) => {
        event.preventDefault;
        const pass1 = document.querySelector(".pass-1").value;
        const pass2 = document.querySelector(".pass-2").value;
        return passCheckAndAlerts(pass1, pass2);
    });
}

function passCheckAndAlerts(pass1, pass2) {
    if (pass1 === pass2 && pass1 !== "" && pass2 !== "") {
        alert("You are welcome");
        if (document.querySelector(".noaccess")) {
            document.querySelector(".noaccess").remove();
        }
    } else {
        if (!document.querySelector(".noaccess")) {
            const denied = document.createElement("p");
            denied.style.color = "red";
            denied.classList.add("noaccess");
            denied.textContent = "Потрібно ввести однакові значення";
            document.querySelectorAll(".input-wrapper")[1].after(denied);
        }
    }
}

getPasses();
switchIconsAndShowPass();
