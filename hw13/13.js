// 1) setTimeout() запускається через вказаний проміжок часу, а setInterval() - так само через вказаний проміжок часу, але циклічно.
// 2) За синтаксисом спочатку буде виконаний код, а потім миттєво спрацює функція, бо затримка нульова.
// 3) В залежності від коду, функція, посилаючись на зовнішнє середовище, може займати відчутну кількість памʼяті, як і зовнішні змінні,
// і коли вона перестає бути потрібною, краще її скасувати задля оптимізації ресурсів.

const images = [...document.querySelectorAll(".image-to-show")];
let count = 0;
let slideTime = setInterval(imageChange, 3000);

const pauseBtn = document.createElement("button");
pauseBtn.innerText = "Припинити";
pauseBtn.style.margin = "20px 45px";
pauseBtn.style.borderRadius = "5px";
pauseBtn.style.backgroundColor = "yellow";
document.body.append(pauseBtn);

const resumeBtn = document.createElement("button");
resumeBtn.innerText = "Відновити показ";
resumeBtn.style.margin = "20px 45px";
resumeBtn.style.borderRadius = "5px";
resumeBtn.style.backgroundColor = "green";
resumeBtn.style.color = "white";

pauseBtn.addEventListener("click", (event) => {
    clearInterval(slideTime);
    document.body.append(resumeBtn);
});
resumeBtn.addEventListener("click", (event) => {
    slideTime = setInterval(imageChange, 3000);
    resumeBtn.remove();
});
function imageChange() {
    let actImage = document.querySelector(".active");
    count++;

    if (count > images.length - 1) {
        actImage = images[0];
        actImage.classList.add("active");
        images[images.length - 1].classList.remove("active");
        count = 0;
    } else {
        actImage.nextElementSibling.classList.add("active");
        actImage.classList.remove("active");
    }
}
