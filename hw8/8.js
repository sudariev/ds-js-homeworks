/* 1) Опишіть своїми словами що таке Document Object Model (DOM)

DOM – це обʼєктана модель докумету, яка весь вміст сторінки представляє у вигляжі обʼєктів, що їх можна змінювати, створювати, видаляти.

2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?

InnerText має вигляд звичайного тексту, в той час як innerHTML містить повний HTML-код елемента, разом із текстом і тегами.

3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Якщо елемент має атрибут id, то до нього можна звернутись через element.getElementById('id'), 
аналогічно, існують запити по тегу, классу і т.д. Але найкращий і найунівесальніший спосіб пошуку елементу за css-селектором
за допомогою element.querySelector(css) та element.querySelectorAll(css) якщо потрібно знайти всі елементи.

*/

const paragraphs = document.querySelectorAll("p");

for (let paragraph of paragraphs) {
    paragraph.style.background = "#ff0000";
}

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const optListParent = optionsList.parentElement;
console.log(optListParent);

const optListChildNodes = optionsList.childNodes;
for (let node of optListChildNodes) {
    console.log(node.nodeName + ": " + node.nodeType);
}

const testParagraph = document.getElementById("testParagraph");
testParagraph.innerHTML = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");

let mainHeaderChildNodes = mainHeader.childNodes;
for (let node of mainHeaderChildNodes) {
    console.log(node);
}
document.querySelector(".main-header").childNodes.className = "nav-item";

let optListTitle = document.querySelectorAll(".options-list-title");
for (let item of optListTitle) {
    item.classList.remove("options-list-title");
}
