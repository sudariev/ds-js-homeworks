//  1) Екранування - використання спеціальних символів в тексті та перетворення їх в звичайний текст,
//  зокрема, якщо вони можуть бути сприйняті як функціональні символи.
//  2) Function declaration statement, function definition expression.
//  3) Hoisting - фактично підняття обʼяви змінних та функції, але не їх ініціалізацій.
//  Таким чином з функціями, завдяки hoisting'у ми можемо викликати функцію до її обʼяви. У випадку зі змінними, якщо вони будуть
//  використані до обʼявлення та ініціалізаціі, то їхнє значення буде undefined.

const createNewUser = () => {
    const newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your surname"),
        birthday: prompt("Enter your birth date in format dd.mm.yyyy"),
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const date = new Date();
            const birth = this.birthday.split(".");
            const birthdate = new Date(birth[2], birth[1] - 1, birth[0]);
            if (
                birthdate.getMonth() < date.getMonth() ||
                (birthdate.getMonth() === date.getMonth() &&
                    birthdate.getDate() <= date.getDate())
            ) {
                return date.getFullYear() - birthdate.getFullYear();
            } else {
                return date.getFullYear() - birthdate.getFullYear() - 1;
            }
        },
        getPassword() {
            return (
                this.firstName[0].toUpperCase() +
                this.lastName.toLowerCase() +
                this.birthday.slice(6)
            );
        },
    };
    return newUser;
};
const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
