document.querySelector(".theme-change").addEventListener("click", () => {
    if (localStorage.getItem("theme-change") === "invert") {
        localStorage.removeItem("theme-change");
    } else {
        localStorage.setItem("theme-change", "invert");
    }
    invertTheme();
});

invertTheme = () => {
    if (localStorage.getItem("theme-change") === "invert") {
        document.querySelector("html").classList.add("invert");
    } else {
        document.querySelector("html").classList.remove("invert");
    }
};

invertTheme();
