// Тому що наразі існують і широко використовуються альтернативні засоби взаємодії
// з інформацією, зокрема: вставка з буферу обміну раніше скопійованих даних,
// голосовий ввід данних, екранна (сенсорна) клавіатура та інші.

const buttons = [...document.getElementsByClassName("btn")];
document.addEventListener("keydown", function (evt) {
    const lighted = buttons.filter(
        (button) =>
            button.innerText === evt.code.slice(3) ||
            button.innerText === evt.key
    );
    const blacked = buttons.filter(
        (button) =>
            button.innerText !== evt.code.slice(3) ||
            button.innerText !== evt.key
    );
    blacked.forEach((button) => (button.style.backgroundColor = "#000000"));
    lighted[0].style.backgroundColor = "#0000ff";
});
